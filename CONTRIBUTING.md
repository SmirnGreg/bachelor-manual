Hello everyone!

This is a guide on how to contribute bachelor helpdesk / issues tracker.


1) Create an issue (https://gitlab.com/SmirnGreg/bachelor-manual/issues) describing your problem.

2) Describe the problem as broad as possible.

3) Manage to provide files to represent the issue, at least their listings. Add a folder in repository and put your files there if possible. Add your issue number after \# to commit message. 