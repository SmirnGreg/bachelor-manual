# bachelor-manual

This repository contains manuals and issue tracker for people using bachelor.mpia.de cluster.

Please read the [contribution guide](CONTRIBUTING.md) first.