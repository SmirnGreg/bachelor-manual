There is a possibility to occupy more than 2gb on queue2g64c with one job.

```python
#memuse.py
import numpy as np
a = np.random.rand(1000000000)
print(a[100:10000])
import time
time.sleep(200)
```
```bash
#memuse.sh
python3 memuse.py
```
```
qsub -cwd -q queue2g64c@bachelor-node05 memuse.sh         
```

This takes approx 7 GB memory on node05.

To limit it, one can use `-l s_vmem=20480M`

Also, without `-pe mpi 1` nothing prevents numpy from occupying all the cores.

Detailed info about processors and memory usage can be reached with:
```
qlogin -q queue2g64c@bachelor-node05 
export TERM=linux
htop
```
