#!/usr/bin/env python3
#heavy.py
"""Stress test on one core"""
import math
import time
import argparse
import random

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fully load one core for given time in sec")
    parser.add_argument("--time", default=200., type=float, help="Time to wait")
    args = parser.parse_args()
    time0 = time.time()
    while True:
        arr = random.random()
        arr = math.sin(arr)
        if time.time() - time0 > args.time:
            break

```
```bash
python3 heavy.py
```
```
qsub -q queue1g1c@bachelor-node01 -t 1:30 -cwd  heavy.sh   
qsub -q queue1g4c@bachelor-node01 -t 1:30 -cwd  heavy.sh   
```


Then on node01:
![image](/uploads/a01065a6d623832379eaeda9df3173d5/image.png)

Also 48 jobs at the moment on qstat.
