#!/usr/bin/env python3
"""Stress test on one core"""
import math
import time
import argparse
import random

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fully load one core for given time in sec")
    parser.add_argument("--time", default=200., type=float, help="Time to wait")
    args = parser.parse_args()
    time0 = time.time()
    while True:
        arr = random.random()
        arr = math.sin(arr)
        if time.time() - time0 > args.time:
            break

